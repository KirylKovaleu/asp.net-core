﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ReSalingTickets.Test
{
    public class EventControllerTest
    {
        [Fact]
        public void IndexReturnsAViewResultWithAListOfEvents()
        {
            // Arrange
            var mock = new Mock<IEventService>();
            mock.Setup(x => x.GetAllEvent()).Returns(GetTestEvents());

            // Act

            // Assert
        }

        private List<EventDTO> GetTestEvents()
        {
            var phones = new List<EventDTO>
            {
                new EventDTO { Id=Guid.NewGuid(), Name="Gomel"},
                new EventDTO { Id=Guid.NewGuid(), Name="Minsk"},
                new EventDTO { Id=Guid.NewGuid(), Name="Moskva"},
                new EventDTO { Id=Guid.NewGuid(), Name="Grodno"},
            };

            return phones;
        }
    }
}
