﻿using ReSalingTickets.Data.Entities;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Data.Implements
{
    public class TicketMemoryRepository : IRepository
    {
       // private static TicketMemoryRepository _instance;

        //private static object _lock = new object();

        private List<Event> _events;
        private List<City> _cities;
        private List<User> _users;
        private List<Order> _orders;
        private List<Venue> _venues;
        private List<Ticket> _tickets;
        
        public TicketMemoryRepository()
        {
            SetInitialData();
        }

        public void Add<T>(IEnumerable<T> items) where T : class, IEntity
        {
            var elemtnts = GetAll<T>().ToList();
            elemtnts.AddRange(items);
        }

        void IRepository.Add<T>(T item)
        {
            if (item is User)
            {
                _users.Add(item as User);
            }
            if (item is Event)
            {
                _events.Add(item as Event);
            }
            if (item is City)
            {
                _cities.Add(item as City);
            }
            if (item is Order)
            {
                _orders.Add(item as Order);
            }
            if (item is Venue)
            {
                _venues.Add(item as Venue);
            }
            if (item is Ticket)
            {
                _tickets.Add(item as Ticket);
            }

        }

        void IRepository.Delete<T>(IEnumerable<T> items)
        {
            throw new NotImplementedException();
        }

        void IRepository.Delete<T>(Guid id)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(Guid id) where T : class, IEntity
        {
            var mas = GetAll<T>();
            var item = mas.ToList().Find(x => x.Id.Equals(id));
            return item;
        }

        public IEnumerable<T> GetAll<T>() where T : class, IEntity
        {
            if (typeof(T).Name == "Event")
            {
                IEnumerable<T> item = _events.Cast<T>();
                return item;
            }
            if (typeof(T).Name == "City")
            {
                IEnumerable<T> item = _cities.Cast<T>();
                return item;
            }
            if (typeof(T).Name == "User")
            {
                IEnumerable<T> item = _users.Cast<T>();
                return item;
            }
            if (typeof(T).Name == "Venue")
            {
                IEnumerable<T> item = _venues.Cast<T>();
                return item;
            }
            if (typeof(T).Name == "Order")
            {
                IEnumerable<T> item = _orders.Cast<T>();
                return item;
            }
            else
            {
                IEnumerable<T> item = _tickets.Cast<T>();
                return item;
            }
        }

        void IRepository.Update<T>(T item)
        {
            List<T> items = GetAll<T>().ToList();
            var index = items.FindIndex(x => x.Id.Equals(item.Id));
            items.RemoveAt(index);
            items.Insert(index, item);
        }

        private void SetInitialData()
        {
            _events = AddEvents();
            _cities = AddCities();
            _users = AddUsers();
            _orders = AddOrders();
            _venues = AddVenues();
            _tickets = AddTickets();
        }

        private List<Ticket> AddTickets()
        {
            return new List<Ticket>()
            {
                new Ticket
                {
                    Id = Guid.Parse("bf21a5cc-1c99-410d-b67a-6104ce9b4c74"),
                    EventId = Guid.Parse("8f7a1039-ff64-4c74-9cc3-183c8ec74d48"),
                    SellerId = Guid.Parse("0a48a0f5-85d3-4b0c-a731-c12d8c006930"),
                    Price = 10,
                    Confirmation = false
                },
                new Ticket
                {
                    Id = Guid.Parse("553b5cc9-bec4-44b9-9a3a-7e0969652c0e"),
                    EventId = Guid.Parse("63dccbd6-ba4d-4bfb-a467-028d22ad4397"),
                    SellerId = Guid.Parse("1a4108f3-f432-435b-95bf-825057d5d710"),
                    Price = 100,
                    Confirmation = true
                },
                new Ticket
                {
                    Id = Guid.Parse("500b9839-7d73-40dd-a5b5-0f8ce00d99ae"),
                    EventId = Guid.Parse("63dccbd6-ba4d-4bfb-a467-028d22ad4397"),
                    SellerId = Guid.Parse("a4ddc111-812b-4c6a-b449-26eceab9b8ba"),
                    Price = 130,
                    Confirmation = true
                },
                    new Ticket
                {
                    Id = Guid.Parse("1fb21f48-abc5-4948-989d-4600e90e8301"),
                    EventId = Guid.Parse("8f7a1039-ff64-4c74-9cc3-183c8ec74d48"),
                    SellerId = Guid.Parse("2890f587-37d6-4bc4-9e77-935fb06cd40a"),
                    Price = 170,
                    Confirmation = false
                },
                    new Ticket
                {
                    Id = Guid.Parse("f18e14a0-2083-4e2d-86a3-c69227d10d9a"),
                    EventId = Guid.Parse("39730a73-35e0-4869-b0d8-e3f2af7b541c"),
                    SellerId = Guid.Parse("0a48a0f5-85d3-4b0c-a731-c12d8c006930"),
                    Price = 60,
                    Confirmation = false
                },
                    new Ticket
                {
                    Id = Guid.Parse("90178135-32db-4e0e-9ebb-4fc6c09a8082"),
                    EventId = Guid.Parse("f56cef6b-c08a-4d24-84c4-3fd1170f7c53"),
                    SellerId = Guid.Parse("a4ddc111-812b-4c6a-b449-26eceab9b8ba"),
                    Price = 12,
                    Confirmation = false
                },
                    new Ticket
                {
                    Id = Guid.Parse("891c9e5a-0619-489e-879c-71d0d044ac35"),
                    EventId = Guid.Parse("15c8d621-8481-4314-8449-de278fc6f6fc"),
                    SellerId = Guid.Parse("1a4108f3-f432-435b-95bf-825057d5d710"),
                    Price = 30,
                    Confirmation = false
                },
                    new Ticket
                {
                    Id = Guid.Parse("bf21a5cc-1c99-410d-b67a-6104ce9b4c78"),
                    EventId = Guid.Parse("8f7a1039-ff64-4c74-9cc3-183c8ec74d48"),
                    SellerId = Guid.Parse("0a48a0f5-85d3-4b0c-a731-c12d8c006930"),
                    Price = 155,
                    Confirmation = false
                }
            };
        }

        private List<Event> AddEvents()
        {
            return new List<Event>()
            {
                    new Event
                {
                    Id = Guid.Parse("8f7a1039-ff64-4c74-9cc3-183c8ec74d48"),
                    Name = "ComedyClab",
                    Date = new DateTime(2016, 12, 29),
                    VenueId = Guid.Parse("9ee368d8-d825-4825-9488-96c7289f6dc0"),
                    PathToTheBanner = "/Comedy.jpg",
                    EventDascription = "<b>Comedy</b>"
                },
                    new Event
                {
                    Id = Guid.Parse("f56cef6b-c08a-4d24-84c4-3fd1170f7c53"),
                    Name = "Концерт Киркорова",
                    Date = new DateTime(2017, 12, 29),
                    VenueId = Guid.Parse("d1e37728-2aa3-47a6-be38-d9b84facfdda"),
                    PathToTheBanner = "/Kirkorov.jpg",
                    EventDascription = "<div>/Comedy.html</div>"
                },
                    new Event
                {
                    Id = Guid.Parse("63dccbd6-ba4d-4bfb-a467-028d22ad4397"),
                    Name = "Концерт Баскова",
                    Date = new DateTime(2018, 12, 29),
                    VenueId = Guid.Parse("763e06b6-5eaa-4f5d-aa07-d627bccba727"),
                    PathToTheBanner = "/Baskov.jpg",
                    EventDascription = "<div>/Comedy.html</div>"
                },
                    new Event
                {
                    Id = Guid.Parse("39730a73-35e0-4869-b0d8-e3f2af7b541c"),
                    Name = "Концерт Победы",
                    Date = new DateTime(2019, 12, 29),
                    VenueId = Guid.Parse("b10293cd-3a5c-4c24-9144-5552dbbc0638"),
                    PathToTheBanner = "/Pobeda.jpg",
                    EventDascription = "<div>/Comedy.html</div>"
                },
                    new Event
                {
                    Id = Guid.Parse("15c8d621-8481-4314-8449-de278fc6f6fc"),
                    Name = "КВН",
                    Date = new DateTime(2018, 10, 29),
                    VenueId = Guid.Parse("86252a0b-b2bd-4b76-b4f4-0ac376eea41f"),
                    PathToTheBanner = "/KVN.jpg",
                    EventDascription = "<div>/Comedy.html</div>"
                },
                    new Event
                {
                    Id = Guid.Parse("ea2fab3e-b58e-455c-a6c0-3e1fb8d46d74"),
                    Name = "Сожский карогод",
                    Date = new DateTime(2016, 12, 29),
                    VenueId = Guid.Parse("3b88f97e-c1b4-459c-999d-f3895cf3de0e"),
                    PathToTheBanner = "/Karagod.jpg",
                    EventDascription = "<div>/Comedy.html</div>"
                }
            };
        }

        private List<Order> AddOrders()
        {
            return new List<Order>()
            {
                    new Order
                {
                    Id = Guid.Parse("755e6229-b3ba-4f7a-8b51-f617c8f1bc7b"),
                    BuyerId = Guid.Parse("2890f587-37d6-4bc4-9e77-935fb06cd40a"),
                    StatusOrder = 1,
                    TicketId = Guid.Parse("bf21a5cc-1c99-410d-b67a-6104ce9b4c74")
                },
                    new Order
                {
                    Id = Guid.Parse("e206b8b9-10af-45ce-9b36-7a666367c030"),
                    BuyerId = Guid.Parse("a4ddc111-812b-4c6a-b449-26eceab9b8ba"),
                    StatusOrder = 0,
                    TicketId = Guid.Parse("553b5cc9-bec4-44b9-9a3a-7e0969652c0e")
                },
                    new Order
                {
                    Id = Guid.Parse("fc695ad7-a994-4c68-bd83-f10670ec3a6a"),
                    BuyerId = Guid.Parse("0a48a0f5-85d3-4b0c-a731-c12d8c006930"),
                    StatusOrder = 1,
                    TicketId = Guid.Parse("500b9839-7d73-40dd-a5b5-0f8ce00d99ae")
                },
                    new Order
                {
                    Id = Guid.Parse("64843a83-6cc6-4a9a-a4a0-c9c2b9575295"),
                    BuyerId = Guid.Parse("1a4108f3-f432-435b-95bf-825057d5d710"),
                    StatusOrder = 1,
                    TicketId = Guid.Parse("1fb21f48-abc5-4948-989d-4600e90e8301")
                },
                    new Order
                {
                    Id = Guid.Parse("fffbf589-b178-44f9-b71c-1a297e3fe4a4"),
                    BuyerId = Guid.Parse("a4ddc111-812b-4c6a-b449-26eceab9b8ba"),
                    StatusOrder = 0,
                    TicketId = Guid.Parse("f18e14a0-2083-4e2d-86a3-c69227d10d9a")
                }
            };
        }

        private List<City> AddCities()
        {
            return new List<City>()
            {
                    new City
                {
                    Id = Guid.Parse("5eb3501e-d685-44e8-bcae-7bc968e2e68c"),
                    Name = "Гомель"
                },
                    new City
                {
                    Id = Guid.Parse("0e7325fd-e3ac-4c30-89d3-884df520f018"),
                    Name = "Брест"

                },
                    new City
                {
                    Id = Guid.Parse("00c8d473-df18-4b6c-a80a-809e35050cc4"),
                    Name = "Москва"

                },
                    new City
                {
                    Id = Guid.Parse("fc5150ad-109b-4517-9d7d-ced5b6b99611"),
                    Name = "Киев"

                },
                    new City
                {
                    Id = Guid.Parse("08f2d074-4331-4a25-a495-cb86406d749e"),
                    Name = "Кишинев"

                },
                    new City
                {
                    Id = Guid.Parse("ffa1cbe3-8c6c-4413-a178-c893326a5c54"),
                    Name = "Тель-Авив"

                }
            };
        }

        private List<Venue> AddVenues()
        {
            return new List<Venue>()
            {
                    new Venue
                {
                    Id = Guid.Parse("9ee368d8-d825-4825-9488-96c7289f6dc0"),
                    Name = "ТЦ",
                    Adress = "Проспект Речицкий",
                    CityId = Guid.Parse("5eb3501e-d685-44e8-bcae-7bc968e2e68c")
                },
                    new Venue
                {
                    Id = Guid.Parse("d1e37728-2aa3-47a6-be38-d9b84facfdda"),
                    Name = "Театр",
                    Adress = "подвал дома",
                    CityId = Guid.Parse("0e7325fd-e3ac-4c30-89d3-884df520f018")

                },
                    new Venue
                {
                    Id = Guid.Parse("763e06b6-5eaa-4f5d-aa07-d627bccba727"),
                    Name = "Стадион",
                    Adress = "За базаром",
                    CityId = Guid.Parse("00c8d473-df18-4b6c-a80a-809e35050cc4")
                },
                    new Venue
                {
                    Id = Guid.Parse("b10293cd-3a5c-4c24-9144-5552dbbc0638"),
                    Name = "Местная забегаловка",
                    Adress = "Где то в центре",
                    CityId = Guid.Parse("fc5150ad-109b-4517-9d7d-ced5b6b99611")
                },
                    new Venue
                {
                    Id = Guid.Parse("86252a0b-b2bd-4b76-b4f4-0ac376eea41f"),
                    Name = "Площадь",
                    Adress = "Окло театра",
                    CityId = Guid.Parse("08f2d074-4331-4a25-a495-cb86406d749e")
                },
                    new Venue
                {
                    Id = Guid.Parse("3b88f97e-c1b4-459c-999d-f3895cf3de0e"),
                    Name = "На пляже",
                    Adress = "У мертвого моря",
                    CityId = Guid.Parse("ffa1cbe3-8c6c-4413-a178-c893326a5c54")
                },
            };
        }

        private List<User> AddUsers()
        {
            var roleAdmin = new Role();
            roleAdmin.role = Roles.Admin;

            var roleUser = new Role();
            roleUser.role = Roles.User;

            return new List<User>()
            {
                    new User
                {
                    Id = Guid.Parse("2890f587-37d6-4bc4-9e77-935fb06cd40a"),
                    Email = "kirya615@gmail.com",
                    Login = "Admin",
                    FirstName = "Кирилл",
                    SecondName = "Ковалев",
                    PasswordHash = "AQAAAAEAACcQAAAAEOzAJ1PfTSpSdAreU+JVIqw5LxTYB2ECJXGN5XHVurnsppRIbgePtAt1VD46e4hBow==",
                    Locale = "ru-RU",
                    Adress = "",
                    NumberPhone = "183-12-33",
                    EmailConfirmed = true,
                    UserRoles = new List<UserRoles>{ new UserRoles {Role= roleAdmin } }
                },
                    new User
                {
                    Id = Guid.Parse("a4ddc111-812b-4c6a-b449-26eceab9b8ba"),
                    Email = "kirya@kirya.ru",
                    Login = "User",
                    FirstName = "Вася",
                    SecondName = "Пупкин",
                    PasswordHash = "AQAAAAEAACcQAAAAEOSBzehdfTRwsQjRYz4qYVUYh0uDYQkh4d0EJroIOPrXb2Qs/EpmHu1nL2punst8eQ==",
                    Locale = "en-GB",
                    Adress = "",
                    NumberPhone = "331-24-19",
                    EmailConfirmed = true,
                    UserRoles = new List<UserRoles>{ new UserRoles {Role= roleUser } }
                },
                    new User
                {
                    Id = Guid.Parse("0a48a0f5-85d3-4b0c-a731-c12d8c006930"),
                    Email = "Moroz@lif.live",
                    Login = "User2",
                    FirstName = "Сярожа",
                    SecondName = "Ковалев",
                    PasswordHash = "User2",
                    Locale = "br-BR",
                    Adress = "",
                    NumberPhone = "288-19-03",
                    EmailConfirmed = true,
                    UserRoles = new List<UserRoles>{ new UserRoles {Role= roleUser } }
                },
                    new User
                {
                    Id = Guid.Parse("1a4108f3-f432-435b-95bf-825057d5d710"),
                    Email = "agsgasg",
                    Login = "User3",
                    FirstName = "Дормидон",
                    SecondName = "Пупкин",
                    PasswordHash = "User3",
                    Locale = "en-GB",
                    Adress = "",
                    NumberPhone = "932-02-56",
                    EmailConfirmed = true,
                    UserRoles = new List<UserRoles>{ new UserRoles {Role= roleUser } }
                }
            };
        }
    }
}