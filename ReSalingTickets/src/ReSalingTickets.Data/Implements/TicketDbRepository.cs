﻿using Microsoft.EntityFrameworkCore;
using ReSalingTickets.Data.Entities;
using ReSalingTickets.Data.Interfaces;
using ReSalingTickets.Data.ReSalingDataBaseContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Data.Implements
{
    public class TicketDbRepository : IRepository
    {
        ReSalingDbContext dbContext;

        public TicketDbRepository(ReSalingDbContext context)
        {
            dbContext = context;

            var users = GetAll<User>();

            if(users.Count() == 0)
            {
                InicializeDataBase();
            }
        }
        public void Add<T>(IEnumerable<T> items) where T : class, IEntity
        {
            dbContext.Set<T>().AddRange(items);
            dbContext.SaveChanges();
        }

        public void Add<T>(T item) where T : class, IEntity
        {
            dbContext.Set<T>().Add(item);
            dbContext.SaveChanges();
        }

        public void Delete<T>(IEnumerable<T> items) where T : class, IEntity
        {
            dbContext.Set<T>().RemoveRange(items);
            dbContext.SaveChanges();
        }

        public void Delete<T>(Guid id) where T : class, IEntity
        {
            var item = dbContext.Set<T>().FirstOrDefault(x => x.Id.Equals(id));
            dbContext.Set<T>().Remove(item);
            dbContext.SaveChanges();
        }

        public T Get<T>(Guid id) where T : class, IEntity
        {
            if (typeof(T) == typeof(User))
            {
                return GetAll<T>().FirstOrDefault(x => x.Id.Equals(id));
            }

                return dbContext.Set<T>().FirstOrDefault(x => x.Id.Equals(id));
        }

        public IEnumerable<T> GetAll<T>() where T : class, IEntity
        {
                var collection = dbContext.Set<T>();

            if(typeof(T) != typeof(User))
            {
                return collection;
            }
            else
            {
                return IncludeVirtualProperties(collection).ToList();
            }
        }

        public void Update<T>(T item) where T : class, IEntity
        {
            var element = dbContext.Set<T>().FirstOrDefault(x => x.Id.Equals(item.Id));
            element = item;
            dbContext.SaveChanges();
        }

        private IEnumerable<T> IncludeVirtualProperties<T>(IEnumerable<T> query) where T : class, IEntity
        {
            var rquery = dbContext.Users.Include(x => x.UserRoles).ThenInclude(f => f.Role).ToList();

            IEnumerable<T> item = rquery.Cast<T>();

            return item;
        }

        public void InicializeDataBase()
        {
            var roleAdmin = new Role();
            roleAdmin.role = Roles.Admin;

            var roleUser = new Role();
            roleUser.role = Roles.User;

            var users = new List<User>()
            {
                    new User
                {
                    Id = Guid.Parse("2890f587-37d6-4bc4-9e77-935fb06cd40a"),
                    Login = "Admin",
                    FirstName = "Кирилл",
                    SecondName = "Ковалев",
                    PasswordHash = "AQAAAAEAACcQAAAAEOzAJ1PfTSpSdAreU+JVIqw5LxTYB2ECJXGN5XHVurnsppRIbgePtAt1VD46e4hBow==",
                    Locale = "ru-RU",
                    EmailConfirmed = true,
                    NumberPhone = "183-12-33",
                    UserRoles = new List<UserRoles>{ new UserRoles {Role= roleAdmin } }
                },
                    new User
                {
                    Id = Guid.Parse("a4ddc111-812b-4c6a-b449-26eceab9b8ba"),
                    Login = "User",
                    FirstName = "Вася",
                    SecondName = "Пупкин",
                    PasswordHash = "AQAAAAEAACcQAAAAEOSBzehdfTRwsQjRYz4qYVUYh0uDYQkh4d0EJroIOPrXb2Qs/EpmHu1nL2punst8eQ==",
                    Locale = "en-GB",
                    EmailConfirmed = true,
                    NumberPhone = "331-24-19",
                    UserRoles = new List<UserRoles>{ new UserRoles {Role= roleUser } }
                }
            };

            Add<User>(users);
        }
    }
}