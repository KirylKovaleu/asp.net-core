﻿using ReSalingTickets.Data.Interfaces;
using System;

namespace ReSalingTickets.Data.Entities
{
    public class City:IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}