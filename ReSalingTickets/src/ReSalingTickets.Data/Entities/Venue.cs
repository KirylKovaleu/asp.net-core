﻿using ReSalingTickets.Data.Interfaces;
using System;

namespace ReSalingTickets.Data.Entities
{
    public class Venue:IEntity 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public Guid CityId { get; set; }
    }
}
