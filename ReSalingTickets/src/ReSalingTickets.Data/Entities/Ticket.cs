﻿using ReSalingTickets.Data.Interfaces;
using System;

namespace ReSalingTickets.Data.Entities
{
    public class Ticket:IEntity
    {
        public Guid Id { get; set; }
        public Guid EventId { get; set; }
        public decimal Price { get; set; }
        public Guid SellerId { get; set; }
        public bool Confirmation { get; set; }
    }
}
