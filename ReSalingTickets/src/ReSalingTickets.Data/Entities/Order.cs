﻿using ReSalingTickets.Data.Interfaces;
using System;

namespace ReSalingTickets.Data.Entities
{
    public class Order:IEntity
    {
        public Guid Id { get; set; }
        public Guid TicketId { get; set; }
        public int StatusOrder { get; set; }
        public Guid BuyerId { get; set; }

    }
}
