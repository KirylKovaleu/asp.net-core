﻿using Autofac;
using AutoMapper;
using ReSalingTickets.Business.BusinessMapperSettings;
using ReSalingTickets.Business.Services.Implementations;
using ReSalingTickets.Business.Services.Interfaces;

namespace ReSalingTickets.Business.Autofac
{
    public class AutofacBusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CityService>().As<ICityService>();
            builder.RegisterType<EventService>().As<IEventService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<RoleService>().As<IRoleService>();
            builder.RegisterType<TicketService>().As<ITicketService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<VenueService>().As<IVenueService>();
            builder.RegisterType<BusinessMapperProfile>().As<Profile>();

            base.Load(builder);
        }
    }
}
