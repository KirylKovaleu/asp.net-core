﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class VenueDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public Guid CityId { get; set; }
    }
}
