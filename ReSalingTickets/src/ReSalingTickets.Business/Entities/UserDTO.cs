﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string PasswordHash { get; set; }
        public string Locale { get; set; }
        public string Adress { get; set; }
        public string NumberPhone { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}
