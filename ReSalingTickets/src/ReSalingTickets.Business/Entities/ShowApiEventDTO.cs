﻿namespace ReSalingTickets.Business.Entities
{
    public class ShowApiEventDTO
    {
        public int Count { get; set; }
        public EventShowDTO eventModel { get; set; }
    }
}
