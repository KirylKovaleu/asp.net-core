﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class EventDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Guid VenueId { get; set; }
        public string PathToTheBanner { get; set; }
        public string EventDascription { get; set; }
    }
}