﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class OrderDTO
    {
        public Guid Id { get; set; }
        public Guid TicketId { get; set; }
        public int StatusOrder { get; set; }
        public Guid BuyerId{ get; set; }
    }
}
