﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class EventShowDTO
    {
        public Guid Id { get; set; }
        public string PathToTheBanner { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string CityName { get; set; }
        public string VenueName { get; set; }
        public string EventDascription { get; set; }
    }
}
