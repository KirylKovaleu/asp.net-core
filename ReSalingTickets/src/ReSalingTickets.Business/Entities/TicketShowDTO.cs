﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class TicketShowDTO
    {
        public Guid Id { get; set; }
        public string PathToTheBanner { get; set; }
        public string EventDescription { get; set; }
        public decimal Price { get; set; }
        public string NameSeller { get; set; }
        public string NameBuyer { get; set; }
        public Guid NameSellerId { get; set; }
        public Guid NameBuyerId { get; set; }
        public bool Confirmation { get; set; }
    }
}
