﻿namespace ReSalingTickets.Business.Services
{
    public enum RepositoryType
    {
        Json,
        Memory,
        DataBase
    }
}