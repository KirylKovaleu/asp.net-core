﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface ICityService 
    {
        IEnumerable<CityDTO> GetAllСities();
        void AddCity(CityDTO city);
        void DeleteCity(Guid id);
        void UpdateCity(CityDTO city);
        CityDTO GetCity(Guid id);
        //void SetInitialDataCity();
    }
}
