﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface IEventService
    {
        IEnumerable<EventDTO> GetAllEvent();
        IEnumerable<EventShowDTO> GetAllShowEvent();
        void AddEvent(EventDTO Event);
        void DeleteEvent(Guid id);
        void UpdateEvent(EventDTO events);
        EventDTO GetEvent(Guid id);
        string GetVenueName(Guid id);
        string GetCityName(Guid id);
        IEnumerable<string> GetTimeForForm(List<string> venues);
        IEnumerable<ShowApiEventDTO> Search(SearchDTO model);
    }
}
