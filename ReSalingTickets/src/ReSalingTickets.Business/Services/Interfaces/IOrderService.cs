﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<OrderDTO> GetAllOrders();
        void AddOrder(OrderDTO order);
        void DeleteOrder(Guid id);
        void UpdateOrder(OrderDTO order);
        OrderDTO GetOrder(Guid id);
        decimal GetTicketPrice(Guid id);
        string GetBuyerName(Guid id);
        string GetEventName(Guid id);
        void EditOrder(Guid id);
        //void SetInitialDataOrders();
    }
}
