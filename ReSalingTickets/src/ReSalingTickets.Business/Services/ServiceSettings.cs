﻿namespace ReSalingTickets.Business.Services
{
    public class ServiceSettings
    {
        public RepositoryType Type { get; set; }

        public string Path { get; set; }
    }
}