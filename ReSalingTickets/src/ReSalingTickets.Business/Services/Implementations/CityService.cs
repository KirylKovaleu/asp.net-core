﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class CityService : BaseService, ICityService 
    {
        public CityService(IRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public void AddCity(CityDTO city)
        {
            if(city != null)
            {
                city.Id = Guid.NewGuid();
                var cityData = _mapper.Map<Data.Entities.City>(city);
                _repository.Add(_mapper.Map<Data.Entities.City>(city));
            }
        }

        public void DeleteCity(Guid id) => _repository.Delete<Data.Entities.City>(id);

        public IEnumerable<CityDTO> GetAllСities()
        {
            var cities = _repository.GetAll<Data.Entities.City>();
            return _mapper.Map<List<CityDTO>>(cities);
        }

        public CityDTO GetCity(Guid id)
        {
            var city = _repository.Get<Data.Entities.City>(id);
            return _mapper.Map<CityDTO>(city);
        }

        public void UpdateCity(CityDTO city)
        {
            var item = _mapper.Map<Data.Entities.City>(city);
            _repository.Update(item);
        }
    }
}
