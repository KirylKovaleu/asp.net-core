﻿using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using System.Collections.Generic;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class StatusOrderService : IStatusOrderService
    {
        public IEnumerable<StatusOrderDTO> GetAllStatus()
        {
            List<StatusOrderDTO> item = new List<StatusOrderDTO>();
            item.Add(StatusOrderDTO.AwaitingApproval);
            item.Add(StatusOrderDTO.Sales);
            return item;
        }
        
    }
}
