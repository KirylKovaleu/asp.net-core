﻿using AutoMapper;
using ReSalingTickets.Data.Interfaces;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class BaseService
    {
        protected readonly IRepository _repository;
        protected readonly IMapper _mapper;

        public BaseService(IRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
    }
}