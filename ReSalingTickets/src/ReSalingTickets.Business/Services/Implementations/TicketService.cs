﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class TicketService : BaseService, ITicketService
    {
        public TicketService(IRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
        public void AddTicket(TicketDTO ticket, string userLogin)
        {
            if(ticket != null && userLogin != null)
            {
                ticket.Confirmation = false;
                ticket.Id = Guid.NewGuid();
                ticket.SellerId = _repository.GetAll<Data.Entities.User>().ToList().Find(x => x.Login.Equals(userLogin)).Id;
                _repository.Add(_mapper.Map<Data.Entities.Ticket>(ticket));
            }
        }

        public void DeleteTicket(Guid id) => _repository.Delete<Data.Entities.Ticket>(id);

        public IEnumerable<TicketDTO> GetAllTickets()
        {
            var tickets = _repository.GetAll<Data.Entities.Ticket>();
            return _mapper.Map<List<TicketDTO>>(tickets);
        }
        
        public string GetBuyerName(Guid id) => _repository.Get<Data.Entities.User>(id).FirstName;

        public string GetEventName(Guid id) => _repository.Get<Data.Entities.Event>(id).Name;

        public IEnumerable<TicketShowDTO> GetMyBuyerTicket(string userName)
        {
            Guid userId = _repository.GetAll<Data.Entities.User>().ToList().Find(x => x.Login.Equals(userName)).Id;
            var orders = _repository.GetAll<Data.Entities.Order>();
            List<TicketShowDTO> items = new List<TicketShowDTO>();

            foreach (var t in orders)
            {
                if (t.BuyerId.Equals(userId))
                {
                    var myBuyerTicket = _repository.GetAll<Data.Entities.Ticket>().ToList().Find(x => x.Id.Equals(t.TicketId));

                    TicketShowDTO item = new TicketShowDTO();
                    
                    item.Id = myBuyerTicket.Id;
                    item.NameSeller = _repository.Get<Data.Entities.User>(myBuyerTicket.SellerId).FirstName;
                    item.Price = myBuyerTicket.Price;
                    item.EventDescription = _repository.Get<Data.Entities.Event>(myBuyerTicket.EventId).EventDascription;
                    item.PathToTheBanner = _repository.GetAll<Data.Entities.Event>().ToList().Find(x => x.EventDascription.Equals(item.EventDescription)).PathToTheBanner;

                    items.Add(item);
                }
            }

            return items;


        }

        public IEnumerable<TicketShowDTO> GetMySellerTicket(string userName) //Список не купленых билетов юзера
        {
            Guid userId = _repository.GetAll<Data.Entities.User>().ToList().Find(x => x.Login.Equals(userName)).Id;
            var myBuyerTicket = _repository.GetAll<Data.Entities.Ticket>();
            List<TicketShowDTO> items = new List<TicketShowDTO>();

            foreach (var t in myBuyerTicket)
            {
                var order = _repository.GetAll<Data.Entities.Order>().ToList().Find(x => x.TicketId.Equals(t.Id));

                if (t.SellerId.Equals(userId) && order == null)
                {
                    TicketShowDTO item = new TicketShowDTO();
                    
                    item.Id = t.Id;
                    item.NameSeller = _repository.Get<Data.Entities.User>(userId).FirstName;
                    item.Price = t.Price;
                    item.EventDescription = _repository.Get<Data.Entities.Event>(t.EventId).EventDascription;

                    items.Add(item);
                }
            }

            return items;

        }

        public IEnumerable<TicketShowDTO> GetMySalesTicket(string userName) //Список проданных билетов юзера
        {
            Guid userId = _repository.GetAll<Data.Entities.User>().ToList().Find(x => x.Login.Equals(userName)).Id;
            var myBuyerTicket = _repository.GetAll<Data.Entities.Ticket>();
            List<TicketShowDTO> items = new List<TicketShowDTO>();

            foreach (var t in myBuyerTicket)
            {
                var order = _repository.GetAll<Data.Entities.Order>().ToList().Find(x => x.TicketId.Equals(t.Id));

                if (order != null)
                {
                    if (t.SellerId.Equals(userId) && order.StatusOrder == (int)StatusOrderDTO.Sales)
                    {
                        TicketShowDTO item = new TicketShowDTO();

                        item.Id = t.Id;
                        item.NameSeller = _repository.Get<Data.Entities.User>(userId).FirstName;
                        item.Price = t.Price;
                        item.EventDescription = _repository.Get<Data.Entities.Event>(t.EventId).EventDascription;
                        item.PathToTheBanner = _repository.GetAll<Data.Entities.Event>().ToList().Find(x => x.EventDascription.Equals(item.EventDescription)).PathToTheBanner;

                        items.Add(item);
                    }
                }
            }

            return items;
        }

        public decimal GetPrice(Guid id) => _repository.Get<Data.Entities.Ticket>(id).Price;

        public string GetSellerName(Guid id) => _repository.Get<Data.Entities.User>(id).FirstName;

        public TicketDTO GetTicket(Guid id)
        {
            var ticket = _repository.Get<Data.Entities.Ticket>(id);
            return _mapper.Map<TicketDTO>(ticket);
        }

        public void UpdateTicket(TicketDTO ticket)
        {
            var item = _mapper.Map<Data.Entities.Ticket>(ticket);
            _repository.Update(item);
        }
        
        public IEnumerable<TicketShowDTO> GetAvilableTickets(Guid Eventid)
        {
            var eventItem = _repository.Get<Data.Entities.Event>(Eventid);
            var tickets = _repository.GetAll<Data.Entities.Ticket>().ToList().FindAll(x => x.EventId.Equals(Eventid));
            List<TicketShowDTO> items = new List<TicketShowDTO>();

            foreach(var t in tickets)
            {
                TicketShowDTO item = new TicketShowDTO();
                var order = _repository.GetAll<Data.Entities.Order>().ToList().Find(x => x.TicketId.Equals(t.Id));

                if (order == null)
                {
                    item.Id = t.Id;
                    item.NameSeller = _repository.Get<Data.Entities.User>(t.SellerId).FirstName;
                    item.Price = t.Price;
                    item.EventDescription = _repository.Get<Data.Entities.Event>(t.EventId).EventDascription;
                    item.PathToTheBanner = eventItem.PathToTheBanner;

                    items.Add(item);
                }
            }

            return items;
        } //Список всех доступных билетов

        public IEnumerable<TicketShowDTO> GetTicketConfirmation(string userName) //Список билетов одидающих подтверждения
        {
            Guid userId = _repository.GetAll<Data.Entities.User>().ToList().Find(x => x.Login.Equals(userName)).Id;
            var items = _repository.GetAll<Data.Entities.Ticket>();
            List<TicketShowDTO> tickets = new List<TicketShowDTO>();

            foreach(var t in items)
            {
                var order = _repository.GetAll<Data.Entities.Order>().ToList().Find(x => x.TicketId.Equals(t.Id));

                if (order != null)
                {
                    if (t.SellerId.Equals(userId) && order.StatusOrder == (int)StatusOrderDTO.AwaitingApproval)
                    {
                        TicketShowDTO item = new TicketShowDTO();

                        item.Id = t.Id;
                        item.NameSeller = _repository.Get<Data.Entities.User>(userId).FirstName;
                        item.NameBuyer = _repository.GetAll<Data.Entities.User>().ToList().Find(x => x.Id.Equals(order.BuyerId)).FirstName;
                        item.Price = t.Price;
                        item.EventDescription = _repository.Get<Data.Entities.Event>(t.EventId).EventDascription;
                        item.PathToTheBanner = _repository.GetAll<Data.Entities.Event>().ToList().Find(x => x.EventDascription.Equals(item.EventDescription)).PathToTheBanner;

                        tickets.Add(item);
                    }
                }
            }

            return tickets;
        }
    }
}
