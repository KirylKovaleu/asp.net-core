﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Entities;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public void AddUser(UserDTO user)
        {
            if (user != null)
            {
                user.Id = Guid.NewGuid();
                var item = _mapper.Map<Data.Entities.User>(user);
                var role = new Role();
                role.role = Roles.User;
                item.UserRoles = new List<UserRoles>
                {
                    new UserRoles
                    {
                        Role = role
                    }
                };
                    _repository.Add(item);
            }
        }

        public void DeleteUsers(Guid id) => _repository.Delete<Data.Entities.User>(id);

        public IEnumerable<UserDTO> GetAllUsers()
        {
            var users = _repository.GetAll<Data.Entities.User>();
            if (users == null || !users.Any())
            {
                return Enumerable.Empty<UserDTO>();
            }

            var us = _mapper.Map<IEnumerable<UserDTO>>(users);
            return us;
        }

        public string GetLocale(Guid id) => _repository.Get<Data.Entities.User>(id).Locale;

        public UserDTO GetUser(string login)
        {
            return GetAllUsers().FirstOrDefault(x => x.Login.Equals(login, StringComparison.OrdinalIgnoreCase));
        }

        public UserDTO GetUser(Guid id) => _mapper.Map<UserDTO>(_repository.Get<Data.Entities.User>(id));

        public string GetUserName(Guid id) => _repository.Get<Data.Entities.User>(id).FirstName;

        public void UpdateUser(UserDTO user)
        {
            var item = _mapper.Map<Data.Entities.User>(user);
            item.UserRoles = _repository.Get<User>(user.Id).UserRoles;
            _repository.Update(item);
        }
    }
}