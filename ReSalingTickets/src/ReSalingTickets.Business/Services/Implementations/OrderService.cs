﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class OrderService : BaseService, IOrderService
    {
        public OrderService(IRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
        public void AddOrder(OrderDTO order)
        {
            order.Id = Guid.NewGuid();
            order.StatusOrder = (int)StatusOrderDTO.AwaitingApproval;

            _repository.Add(_mapper.Map<Data.Entities.Order>(order));
        }

        public void EditOrder(Guid id)
        {
            var order = _repository.GetAll<Data.Entities.Order>().ToList().Find(x => x.TicketId.Equals(id));
            order.StatusOrder = (int)StatusOrderDTO.Sales;

            _repository.Update(order);
        }

        public void DeleteOrder(Guid id) => _repository.Delete<Data.Entities.Order>(id);

        public IEnumerable<OrderDTO> GetAllOrders()
        {
            var orders = _repository.GetAll<Data.Entities.Order>();
            return _mapper.Map<List<OrderDTO>>(orders);
        }

        public string GetBuyerName(Guid id) => _repository.Get<Data.Entities.User>(id).FirstName;

        public string GetEventName(Guid id) => _repository.Get<Data.Entities.Event>(id).Name;

        public OrderDTO GetOrder(Guid id)
        {
            var order = _repository.Get<Data.Entities.Order>(id);
            return _mapper.Map<OrderDTO>(order);
        }

         public decimal GetTicketPrice(Guid id) => _repository.Get<Data.Entities.Ticket>(id).Price;

        public void UpdateOrder(OrderDTO order)
        {
            var item = _mapper.Map<Data.Entities.Order>(order);
            _repository.Update(item);
        }
    }
}