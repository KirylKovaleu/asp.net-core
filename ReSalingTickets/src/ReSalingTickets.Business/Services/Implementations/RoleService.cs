﻿using AutoMapper;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Entities;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class RoleService : BaseService, IRoleService
    {
        public RoleService(IRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
        public void AddUserRole(Guid userId, List<string> roles)
        {
            var user = _repository.Get<User>(userId);
            var addedRoles = new List<UserRoles>();

            foreach (var role in roles)
            {
                var userRole = new Role { role = (Roles)Enum.Parse(typeof(Roles), role) };
                addedRoles.Add(new UserRoles { Role = userRole });
            }

            user.UserRoles = new List<UserRoles>(addedRoles);

            _repository.Update(user);
        }

        public IEnumerable<string> GetUserRoles(Guid userId)
        {
            var user = _repository.Get<User>(userId);
            var roles = user.UserRoles.Select(x => x.Role.role);
            return roles.Select(x => x.ToString("G"));
        }

        public IEnumerable<string> GetAllRoles()
        {
            List<string> roles = new List<string>
            {
                Roles.Admin.ToString("G"),
                Roles.Anonimus.ToString("G"),
                Roles.User.ToString("G")
            };

            return roles;
        }
    }
}