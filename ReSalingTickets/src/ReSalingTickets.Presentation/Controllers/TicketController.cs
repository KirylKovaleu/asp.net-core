﻿using Autofac;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Presentation.Controllers
{
    public class TicketController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private ITicketService _ticketService;
        private IEventService _eventService;
        private readonly IMapper _mapper;

        public TicketController(IOptions<PathModel> options,
            IComponentContext componentContext,
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IMapper mapper)
        {
            _mapper = mapper;
            _Resourcelocalizer = Resourcelocalizer;
            _ticketService = serviceFactory.CreateTicketService();
            _eventService = serviceFactory.CreateEventService();
        }
        
        
        public IActionResult GetllAvailableTickets(Guid id)
        {
            var tickets = _mapper.Map<List<TicketShowModel>>(_ticketService.GetAvilableTickets(id));
            if (tickets.Count() != 0)
            {
                ViewBag.image = tickets[0].PathToTheBanner;
                ViewBag.Description = tickets[0].EventDescription;
            }

            return View(tickets);
        }
        
        [HttpGet]
        [Authorize]
        public IActionResult AddTicket()
        {
            var model = new AddTicketsViewModel
                            {
                                Events = GetEventSelectList()
                            };

            return View(model);
        }

        [HttpPost]
        public IActionResult AddTicket(AddTicketsViewModel ticketModel)
        {
            if (ModelState.IsValid)
            {
                var userLogin = User.Identity.Name;

                _ticketService.AddTicket(_mapper.Map<TicketDTO>(ticketModel.Ticket), userLogin);
                return RedirectToAction("UserPanel", "User");
            }
            else
            {
                var model = new AddTicketsViewModel
                {
                    Events = GetEventSelectList(),
                    Ticket = ticketModel.Ticket
                };

                return View(model);
            }
        }
        
        [Authorize]
        public void Delete(int id)
        {
        }

        public IEnumerable<SelectListItem> GetEventSelectList()
        {
            var items = _eventService.GetAllEvent();

            var events = items
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });

            return events;
        }
    }
}
