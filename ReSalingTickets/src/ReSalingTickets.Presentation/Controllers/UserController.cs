﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using System;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Controllers
{
    public class UserController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private UserManager<UserModel> _userManager;
        private ITicketService _ticketService;

        public UserController(
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory)
        {
            _Resourcelocalizer = Resourcelocalizer;
            _userManager = serviceFactory.CreateUserManager();
            _ticketService = serviceFactory.CreateTicketService();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult AdminPanel()
        {
            return View();
        }
        
        [Authorize]
        public IActionResult UserPanel()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            var users = _userManager.Users;
            return View(users);
        }

        [HttpGet]
        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create(UserModel user)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(user);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            return View(user);
        }
        
        [HttpPost]
        public async Task<IActionResult> Edit(UserModel user)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            UserModel user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> ChagePassword(Guid id)
        {
            UserModel user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return NotFound();
            }
            ChagePasswordModel model = new ChagePasswordModel { id = Guid.Parse(user.Id), Login = user.UserName };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChagePassword(ChagePasswordModel item)
        {
            UserModel user = await _userManager.FindByIdAsync(item.id.ToString());
            if (user != null)
            {
                var _passwordValidator =
                        HttpContext.RequestServices.GetService(typeof(IPasswordValidator<UserModel>)) as IPasswordValidator<UserModel>;
                var _passwordHasher =
                    HttpContext.RequestServices.GetService(typeof(IPasswordHasher<UserModel>)) as IPasswordHasher<UserModel>;

                IdentityResult result =
                    await _passwordValidator.ValidateAsync(_userManager, user, item.NewPassword);
                if (result.Succeeded)
                {
                    user.PasswordHash = _passwordHasher.HashPassword(user, item.NewPassword);
                    await _userManager.UpdateAsync(user);
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Пользователь не найден");
            }
            return View(item);
        }

        public async Task<IActionResult> Contact(Guid id)
        {
            var ticket = _ticketService.GetTicket(id);
            UserModel user = await _userManager.FindByIdAsync(ticket.SellerId.ToString());

            return View(user);
        }
    }
}
