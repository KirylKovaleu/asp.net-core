﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Implementations;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.ViewModels;
using Sakura.AspNetCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ReSalingTickets.Presentation.Controllers
{
    public class ApiEventController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private IEventService _eventService;
        private IVenueService _venueService;
        private ICityService _cityService;
        private readonly IMapper _mapper;
        public int count = 0;

        public ApiEventController(
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IMapper mapper)
        {
            _Resourcelocalizer = Resourcelocalizer;
            _eventService = serviceFactory.CreateEventService();
            _venueService = serviceFactory.CreateVenueService();
            _cityService = serviceFactory.CreateCityService();
            _mapper = mapper;
        }

        // GET api/values/5
        [HttpGet]
        [Route("apievent/GetEvent")]
        [Produces(typeof(IEnumerable<EventShowDTO>))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<EventShowDTO>))]
        public IActionResult GetAllEvent([FromQuery]string orderSort, [FromQuery]string searchString, [FromQuery]int? page)
        {
            ViewBag.NameSortParm = string.IsNullOrEmpty(orderSort) ? "name" : "";
            ViewBag.CitySortParm = orderSort == "City" ? "city_desc" : "City";
            ViewBag.VenueSortParm = orderSort == "Venue" ? "venue_desc" : "Venue";
            ViewBag.VenueSortParm = orderSort == "Date" ? "date_desc" : "Date";

            var events =  _eventService.GetAllShowEvent();

            switch (orderSort)
            {
                case "name":
                    events = events.OrderByDescending(s => s.Name);
                    break;
                case "City":
                    events = events.OrderBy(s => s.CityName);
                    break;
                case "city_desc":
                    events = events.OrderByDescending(s => s.CityName);
                    break;
                case "Venue":
                    events = events.OrderBy(s => s.VenueName);
                    break;
                case "venue_desc":
                    events = events.OrderByDescending(s => s.VenueName);
                    break;
                case "Date":
                    events = events.OrderBy(s => s.VenueName);
                    break;
                case "date_desc":
                    events = events.OrderByDescending(s => s.VenueName);
                    break;
                default:
                    events = events.OrderBy(s => s.Name);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return new SerializeResponceResult(events.ToPagedList(pageSize, pageNumber));
        }

        [HttpGet]
        [Route("apievent/GetCities")]
        [Produces(typeof(List<CityModel>))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(List<CityModel>))]
        public IActionResult GetAllCities()
        {
            count++;
            var cities = _cityService.GetAllСities();
            var ss = _mapper.Map<List<CityModel>>(cities);
            return new SerializeResponceResult(ss);
        }

        [HttpGet]
        [Route("apievent/GetVenues")]
        [Produces(typeof(IEnumerable<string>))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<string>))]
        public IActionResult GetAllVenues([FromQuery][ModelBinder]List<string> cities)
        {
            var venues = _venueService.GetVenueForForm(cities);
            return new SerializeResponceResult(venues);
        }

        [HttpGet]
        [Route("apievent/GetDate")]
        [Produces(typeof(IEnumerable<string>))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<string>))]
        public IActionResult GetAllDate([FromQuery][ModelBinder]List<string> venues)
        {
            var dateTimes = _eventService.GetTimeForForm(venues);
            return new SerializeResponceResult(dateTimes);
        }

        [HttpGet]
        [Route("apievent/Search")]
        [Produces(typeof(IEnumerable<ShowApiEventDTO>))]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<ShowApiEventDTO>))]
        public IActionResult Search([FromQuery]SearchModel model)
        {
            var result = _eventService.Search(_mapper.Map<SearchDTO>(model));

            if (result != null)
            {
                result = result.OrderBy(x => x.Count);
            }

            return new SerializeResponceResult(result);
        }

    }
}
