﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.Controllers
{
    public class CityController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private ICityService _cityRepository;
        private readonly IMapper _mapper;
        
        public CityController(
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IMapper mapper)
        {
            _mapper = mapper;
            _Resourcelocalizer = Resourcelocalizer;
            _cityRepository = serviceFactory.CreateCityService();
        }

        [HttpGet]
        [Authorize(Roles ="Admin")]
        public IActionResult AddCity()
        {
            CityModel city = new CityModel();
            return View(city);
        }

        [HttpPost]
        public IActionResult AddCity(CityModel city)
        {
            if (ModelState.IsValid)
            {
                city.Id = Guid.NewGuid();
                _cityRepository.AddCity(_mapper.Map<CityDTO>(city));
            }
            return RedirectToAction("AdminPanel","User");
        }

        public IActionResult ShowAllCities()
        {
            var cc = _cityRepository.GetAllСities();
            var t = _mapper.Map<List<CityModel>>(cc);
            var cities = _mapper.Map<List<CityModel>>(_cityRepository.GetAllСities());
            return View(cities);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteCity(Guid id)
        {
            var city = _mapper.Map<CityModel>(_cityRepository.GetCity(id));
            return View(city);
        }

        [HttpPost]
        public IActionResult DeleteCity(CityModel city)
        {
            _cityRepository.DeleteCity(city.Id);
            return View();
        }
    }
}
