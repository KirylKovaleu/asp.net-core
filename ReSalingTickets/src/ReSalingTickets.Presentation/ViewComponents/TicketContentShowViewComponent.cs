﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.Models.Enums;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.ViewComponents
{
    [ViewComponent(Name = "TicketShowComponent")]
    public class TicketContentShowViewComponent : ViewComponent
    {
        private readonly IServiceFactory _serviceFactory;
        private readonly IMapper _mapper;

        public TicketContentShowViewComponent(IServiceFactory serviceFactory, IMapper mapper)
        {
            _mapper = mapper;
            _serviceFactory = serviceFactory;
        }

        [Authorize]
        public IViewComponentResult Invoke(TubEnum result)
        {
            var ticketService = _serviceFactory.CreateTicketService();

            switch (result)
            {
                case TubEnum.First:
                    {
                        string userLogin = User.Identity.Name;
                        var tickets = _mapper.Map<List<TicketShowModel>>(ticketService.GetMySellerTicket(userLogin));

                        return View("_ShowAllMySellerTicket", tickets);
                    }
                case TubEnum.Second:
                    {
                        string userLogin = User.Identity.Name;
                        var tickets = _mapper.Map<List<TicketShowModel>>(ticketService.GetTicketConfirmation(userLogin));

                        return View("_ShowTicketConfirmation", tickets);
                    }
                case TubEnum.Third:
                    {
                        string userLogin = User.Identity.Name;
                        var tickets = _mapper.Map<List<TicketShowModel>>(ticketService.GetMySalesTicket(userLogin));

                        return View("_ShowTicketSales", tickets);
                    }
                case TubEnum.Fourth:
                    {
                        string userLogin = User.Identity.Name;
                        var tickets = _mapper.Map<List<TicketShowModel>>(ticketService.GetMyBuyerTicket(userLogin));

                        return View("_ShowAllMyBuyerTicket", tickets);
                    }
                default: break;
            }
            return null;
        }
    }
}
