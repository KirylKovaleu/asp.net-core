﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Filters
{
    public class CustomUserStore : IUserStore<UserModel>,
        IUserPasswordStore<UserModel>,
        IUserClaimStore<UserModel>,
        IQueryableUserStore<UserModel>
    {
        private readonly IRoleService _roleService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public CustomUserStore(IUserService userService, IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _userService = userService;
            _mapper = mapper;
        }

        public IQueryable<UserModel> Users
        {
            get
            {
                return _mapper.Map<List<UserModel>>(_userService.GetAllUsers()).AsQueryable();
            }
        }
    
        public Task<string> GetUserIdAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id);
        }

        public Task<string> GetUserNameAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Login);
        }

        public Task SetUserNameAsync(UserModel user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetNormalizedUserNameAsync(UserModel user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetNormalizedUserNameAsync(UserModel user, string normalizedName, CancellationToken cancellationToken)
        {
            user.Login = normalizedName;
            return Task.FromResult(true);
        }

        public Task<IdentityResult> CreateAsync(UserModel user, CancellationToken cancellationToken)
        {
            var item = _mapper.Map<UserDTO>(user);
            item.PasswordHash = new PasswordHasher<UserModel>().HashPassword(user, user.Password);
            item.Id = Guid.Empty;
            _userService.AddUser(item);
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> UpdateAsync(UserModel user, CancellationToken cancellationToken)
        {
            _userService.UpdateUser(_mapper.Map<UserDTO>(user));
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(UserModel user, CancellationToken cancellationToken)
        {
            _userService.DeleteUsers(Guid.Parse(user.Id));
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<UserModel> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<UserModel>(_userService.GetUser(Guid.Parse(userId)));
            return Task.FromResult(user);
        }

        public Task<UserModel> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var existUsers = _userService.GetUser(normalizedUserName);
            return Task.Run(() =>
            {
                var existUser = _userService.GetUser(normalizedUserName);
                return existUser != null ? _mapper.Map<UserModel>(existUser) : null;                
            });    
        }
        
        public Task SetPasswordHashAsync(UserModel user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(true);
        }

        public Task<string> GetPasswordHashAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }
        
        public Task<bool> HasPasswordAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }
        
        public void Dispose()
        {
        }

        public Task<IList<Claim>> GetClaimsAsync(UserModel user, CancellationToken cancellationToken)
        {
            var roles = _roleService.GetUserRoles(Guid.Parse(user.Id));
            IList<Claim> claims = roles.Select(x => new Claim(ClaimTypes.Role, x.ToString())).ToList();
            return Task.FromResult(claims);
        }

        public Task AddClaimsAsync(UserModel user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ReplaceClaimAsync(UserModel user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimsAsync(UserModel user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IList<UserModel>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}