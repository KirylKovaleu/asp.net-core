﻿using Microsoft.AspNetCore.Mvc.Formatters;
using ReSalingTickets.Business.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Infrastructure.Implementations
{
    public static class TypeExtensions
    {
        internal static bool IsString(this Type type)
        {
            return type.Name == "String";
        }

        internal static bool IsPrimitive(this PropertyInfo info)
        {
            return info.PropertyType.GetTypeInfo().IsPrimitive;
        }
    }

    public class CsvSerializer : OutputFormatter
    {
        public CsvSerializer()
        {
        }

        public CsvSerializer(Type type)
        {
            var _type = type.GetType();

            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance
                | BindingFlags.GetProperty | BindingFlags.SetProperty);

            var q = properties.AsQueryable();

            q = q.Where(a => a.PropertyType.GetTypeInfo().IsValueType || a.PropertyType.Name == "String");

            var r = from a in q
                    orderby a.Name
                    select a;

            _properties = r.ToList();
        }

        private List<PropertyInfo> _properties { get; set; }

        private string _newlineReplacement = ((char)0x254).ToString();

        public string Replacement = ((char)0x255).ToString();

        private string _rowNumberColumnTitle = "RowNumber";

        public char Separator = ';';
        
        public IEnumerable<T> Deserialize<T>(StreamWriter stream) where T : class
        {
            throw new NotImplementedException();
        }
        
        public void Serialize(StreamWriter stream, object data)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeader());

            var result = new List<string>();

            GetPropsData(data, result);

            sb.AppendLine(string.Join(Separator.ToString(), result.ToArray()));

            stream.Write(sb.ToString().Trim());
        }

        private void GetPropsData(object data, List<string> result)
        {
            var values = new List<string>();

            if (data == null)
            {
                return;
            }

            var dataType = data.GetType();
            var isString = dataType.IsString();

            if (data is IEnumerable)
            {
                if (isString)
                {
                    var value = data == null ? "" :
                                data.ToString().Replace(Separator.ToString(), Replacement);
                    result.Add(value);
                }
                else
                {
                    foreach (object o in (data as IEnumerable))
                    {
                        GetPropsData(o, result);
                    }
                }
            }
            else
            {
                var propertyList = dataType.GetProperties();
                if (propertyList.Count() > 0)
                    foreach (var p in propertyList)
                    {
                        var visD = p.IsPrimitive();

                        if (p.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                        {
                            GetPropsData(p.GetValue(data), result);
                        }
                        else if(visD)
                        {
                            var raw = p.GetValue(data);
                            var value = raw == null ? "" :
                                raw.ToString()
                                .Replace(Separator.ToString(), Replacement)
                                .Replace(Environment.NewLine, _newlineReplacement);
                            
                                value = string.Format("\"{0}\"", value);

                            result.Add(value);
                        }
                        else
                        {
                            var r = p.GetValue(data);
                            foreach (var s in r.GetType().GetProperties())
                            {
                                if (s.GetValue(r) != null)
                                {
                                    var res = s.GetValue(r).ToString();
                                    values.Add(res == null ? "" :
                                     res.ToString()
                                    .Replace(Separator.ToString(), Replacement)
                                    .Replace(Environment.NewLine, _newlineReplacement));
                                }
                            }

                            result.AddRange(values);
                        }
                    }
                else
                {
                    var value = data == null ? "" :
                                data.ToString()
                                .Replace(Separator.ToString(), Replacement)
                                .Replace(Environment.NewLine, _newlineReplacement);

                    result.Add(value);
                }
            }
        }

        private string GetHeader()
        {
            var header = _properties.Select(a => a.Name);
            
                header = new string[] { _rowNumberColumnTitle }.Union(header);

            return string.Join(Separator.ToString(), header.ToArray());
        }

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {
            throw new NotImplementedException();
        }
    }
}
