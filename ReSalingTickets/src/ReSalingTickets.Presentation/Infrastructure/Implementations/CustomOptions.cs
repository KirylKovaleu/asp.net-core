﻿using Microsoft.Extensions.Options;
using ReSalingTickets.Presentation.Models;
using System.Collections.Generic;
using System.Reflection;

namespace ReSalingTickets.Presentation.Infrastructure.Implementations
{
    public class CustomOptions
    {
        public readonly IDictionary<string, string> _connectionInfo;
        public CustomOptions(IOptions<PathModel> options)
        {
            _connectionInfo = GetAllInfo(options);
        }

        private IDictionary<string, string> GetAllInfo(IOptions<PathModel> options)
        {
            var values = options.Value;
            var properties = values.GetType().GetProperties();

            var result = new Dictionary<string, string>();

            foreach (var propFromValue in properties)
            {
                var innerDictionaryInfo = new Dictionary<string, string>();
                
                var tempProp = propFromValue.GetValue(values, null);

                if (tempProp != null)
                {
                    var innerProperties = tempProp.GetType().GetProperties();

                    foreach (var innerPropertiy in innerProperties)
                    {
                        var path = innerPropertiy.GetValue(tempProp, null);
                        result.Add(propFromValue.Name, path != null ? path.ToString() : string.Empty);
                    }
                }
            }
            return result;
        }
    }
}
