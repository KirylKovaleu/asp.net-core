﻿using Microsoft.AspNetCore.Identity;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Models;

namespace ReSalingTickets.Presentation.Infrastructure.Interfaces
{
    public interface IServiceFactory
    {
        IUserService CreateUserService();

        IRoleService CreateRoleService();

        UserManager<UserModel> CreateUserManager();

        IConnectionService CreateConnectionService();

        ICityService CreateCityService();

        IEventService CreateEventService();

        ITicketService CreateTicketService();

        IOrderService CreateOrderService();

        IVenueService CreateVenueService();
    }
}