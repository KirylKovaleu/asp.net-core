﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.Infrastructure.Interfaces
{
    public interface IConnectionService
    {
        List<SelectListItem> GetDatabaseType();

        string GetPathConnections(string type);

        string GetTypeConnections(RouteData routeData);
    }
}
