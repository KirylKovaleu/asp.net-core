﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ReSalingTickets.Presentation.Models;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.ViewModels
{
    public class AddTicketsViewModel
    {
        public TicketModel Ticket { get; set; }

        public IEnumerable<SelectListItem> Events { get; set; }
    }
}