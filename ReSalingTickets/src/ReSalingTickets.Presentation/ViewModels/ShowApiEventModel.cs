﻿using ReSalingTickets.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.ViewModels
{
    public class ShowApiEventModel
    {
        public int Count { get; set; }
        public EventShowModel eventModel { get; set; }
    }
}
