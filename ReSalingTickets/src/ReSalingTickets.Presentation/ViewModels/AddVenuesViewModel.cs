﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ReSalingTickets.Presentation.Models;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.ViewModels
{
    public class AddVenuesViewModel
    {
        public VenueModel venueModel { get; set; }

        public IEnumerable <SelectListItem> Cities { get; set; }
    }
}
