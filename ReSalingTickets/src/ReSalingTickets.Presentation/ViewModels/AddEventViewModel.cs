﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ReSalingTickets.Presentation.Models;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.ViewModels
{
    public class AddEventViewModel
    {
        public EventModel Event { get; set; }
        public IEnumerable<SelectListItem> Venues { get; set; }
    }
}
