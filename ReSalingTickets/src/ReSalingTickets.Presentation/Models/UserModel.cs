﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace ReSalingTickets.Presentation.Models
{
    public class UserModel : IdentityUser
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Password { get; set; }
        public string Locale { get; set; }
        public string Adress { get; set; }
        public string NumberPhone { get; set; }
    }
}