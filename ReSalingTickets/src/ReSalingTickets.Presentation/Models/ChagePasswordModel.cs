﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Models
{
    public class ChagePasswordModel
    {
        public Guid id { get; set; }
        public string Login { get; set; }
        public string NewPassword { get; set; }
    }
}
