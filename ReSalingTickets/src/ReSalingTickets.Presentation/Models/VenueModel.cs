﻿using System;

namespace ReSalingTickets.Presentation.Models
{
    public class VenueModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public Guid CityId { get; set; }
    }
}
