﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Models
{
    public class PathModel
    {
        public DataBase DataBase { get; set; }
        public Json Json { get; set; }
        public Memory Memory { get; set; }
        public string PathToFile { get; set; }
        public string Type { get; set; }
    }

    public class Json
    {
        public string PathToFile { get; set; }
    }
    public class DataBase
    {
        public string DefaultConnection { get; set; }
    }

    public class Memory
    {
        public string PathToFile { get; set; }
    }
}
