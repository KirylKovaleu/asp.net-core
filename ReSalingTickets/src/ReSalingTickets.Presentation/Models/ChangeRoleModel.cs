﻿using System.Collections.Generic;

namespace ReSalingTickets.Presentation.Models
{
    public class ChangeRoleModel
    {
        public string UserId { get; set; }
        public string Login { get; set; }
        public List<string> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }

        public ChangeRoleModel()
        {
            AllRoles = new List<string>();
            UserRoles = new List<string>();
        }
    }
}
