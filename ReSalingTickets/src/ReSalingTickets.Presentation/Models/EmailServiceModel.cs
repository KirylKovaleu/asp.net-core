﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Models
{
    public class EmailServiceModel
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSsl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AddresFrom { get; set; }
        public string LetterHeader { get; set; }
    }
}
