﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReSalingTickets.Presentation.Models
{
    public class EventShowModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Banner")]
        public string PathToTheBanner { get; set; }
        [Display(Name = "EventName", ResourceType = typeof(SharedResource))]
        public string Name { get; set; }
        [Display(Name = "Date", ResourceType = typeof(SharedResource))]
        public DateTime Date { get; set; }
        [Display(Name = "City", ResourceType = typeof(SharedResource))]
        public string CityName { get; set; }
        [Display(Name = "Venue", ResourceType = typeof(SharedResource))]
        public string VenueName { get; set; }
        [Display(Name = "Description", ResourceType = typeof(SharedResource))]
        public string EventDescription { get; set; }
    }
}
