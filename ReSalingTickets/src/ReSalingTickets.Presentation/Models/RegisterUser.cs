﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReSalingTickets.Presentation.Models
{
    public class RegisterUser
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        //[Required]
        [Compare("Password", ErrorMessage = "PaswworNotConfim")]
        [DataType(DataType.Password)]
        [Display(Name = "ПодтверждениеПароля")]
        public string PasswordConfirm { get; set; }
        public DateTime DateRegistration { get; set; }
    }
}
