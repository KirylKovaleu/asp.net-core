﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReSalingTickets.Presentation.Models
{
    public class CityModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Город")]
        public string Name { get; set; }
    }
}
