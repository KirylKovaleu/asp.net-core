﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using ReSalingTickets.Presentation.Models;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Services
{
    public class EmailService: IEmailSender
    {
        private EmailServiceModel _options;
        public EmailService(IOptions<EmailServiceModel> options)
        {
            _options = options.Value;
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(_options.LetterHeader, _options.AddresFrom));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_options.Host, _options.Port, _options.UseSsl);
                await client.AuthenticateAsync(_options.UserName, _options.Password);

                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}