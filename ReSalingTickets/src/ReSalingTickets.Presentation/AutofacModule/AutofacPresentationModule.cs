﻿using Autofac;
using AutoMapper;
using ReSalingTickets.Presentation.PresentationMapperSettings;
using System.Collections.Generic;

namespace ReSalingTickets.Presentation.AutofacModule
{
    public class AutofacPresentationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PresentationMapperProfile>().As<Profile>();

            builder.Register(c => new MapperConfiguration(cfg =>
            {
                var profiles = c.Resolve<IEnumerable<Profile>>();
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }

            })).AsSelf().SingleInstance();

            builder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper()).As<IMapper>();

            base.Load(builder);
        }
    }
}